package fil.sr.utils;

import static org.junit.Assert.*;

import org.junit.Test;

public class UtilsTest {

	@Test
	public void testGetip() {
		String pasv = "89,133,150,29,73,26";
		String ip = Utils.getip(pasv);
		assertTrue(ip.equals("89.133.150.29"));
	}

	@Test
	public void testGetPort() {
		String pasv = "89,133,150,29,73,26";
		int port = Utils.getPort(pasv);
		assertTrue(port == (73 * 256) + 26);
	}

	@Test
	public void testParsePASV() {
		String pasv = "227 Entering Passive Mode (89,133,150,29,73,26)";
		String res = Utils.parsePASV(pasv);
		assertTrue("89,133,150,29,73,26".equals(res));
	}

}
