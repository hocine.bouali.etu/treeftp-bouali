package fil.sr.client;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ListResponseTest {

	private ListResponse link;
	private ListResponse dir;
	private ListResponse file;

	@Before
	public void setupBefore() {
		link = new ListResponse(
				"lrwxrwxrwx    1 ftp      ftp            28 Jun 14  2011 MPlayer -> mirrors/mplayerhq.hu/MPlayer");

		dir = new ListResponse("drwxrwxrwx    1 ftp      ftp            4096 Jun 14  2011 pub");

		file = new ListResponse("-rwxrwxrwx    1 ftp      ftp            28 Jun 14  2011 fichier");

	}


	@Test
	public void testIsLink() {

		assertTrue(link.isLink());
		assertFalse(dir.isLink());
		assertFalse(file.isLink());
	}

	@Test
	public void testIsRepository() {
		assertTrue(dir.isRepository());
		assertFalse(link.isRepository());
		assertFalse(file.isRepository());
	}

	@Test
	public void testIsFile() {
		assertTrue(file.isFile());
		assertFalse(link.isFile());
		assertFalse(dir.isFile());
	}


	@Test
	public void testGetFileName() {
		assertTrue(dir.getFileName().equals("pub"));
		assertTrue(file.getFileName().equals("fichier"));
		assertTrue(link.getFileName().equals(" MPlayer -> mirrors/mplayerhq.hu/MPlayer"));
	}

	

}
