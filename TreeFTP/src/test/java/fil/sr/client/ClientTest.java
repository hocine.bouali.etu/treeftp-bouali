package fil.sr.client;

import java.io.IOException;
import java.net.UnknownHostException;

import org.junit.Before;
import org.junit.Test;

import fil.sr.exception.AuthentificationServerException;

public class ClientTest {
	private Client c2;
	private Authentification auth;
	private Authentification falseAuth;

	@Before
	public void setupBefore() {
		auth = new Authentification();
		falseAuth = new Authentification("toto", "titi");

	}

	@Test(expected = UnknownHostException.class)
	public void testConnectWhenfalseAdress() throws UnknownHostException, IOException, AuthentificationServerException {
		c2 = new Client(auth);
		c2.connect("falseadress.com", 21);
	}

	@Test(expected = AuthentificationServerException.class)
	public void testConnectWhenFalseUser() throws UnknownHostException, IOException, AuthentificationServerException {
		c2 = new Client(falseAuth);
		c2.connect("ftp.ubuntu.com", 21);
	}
}