/**
 * Les classes du package Utils permettent de faciliter la réutilisation de
 * methodes communes
 * 
 * @author Bouali Hocine
 *
 */
package fil.sr.utils;