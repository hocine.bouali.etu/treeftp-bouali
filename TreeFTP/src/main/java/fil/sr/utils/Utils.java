package fil.sr.utils;

/**
 * Classe qui contient des methodes permettant la bonne implementation de la
 * commande TreeFTP
 * 
 * @author Bouali Hocine
 *
 */
public class Utils {

	/**
	 * Methode qui parse le retour de la commande PASV et recupere l'adresse IP
	 * 
	 * @param line retour de la commande FTP PASV nettoye, la ligne ne contient que
	 *             ce qu'il y'a entre parentheses
	 * @return l'adresse IP reconstitue
	 */
	public static String getip(String line) {
		String[] tab = line.split(",");
		return tab[0] + "." + tab[1] + "." + tab[2] + "." + tab[3];
	}

	/**
	 * Methode qui parse le retour de la commande PASV nettoye du debut de la chaine
	 * et calcule le nouveau port
	 * 
	 * @param line retour de la commande PASV nettoye , la ligne ne contient que ce
	 *             qu'il y'a entre parenthese
	 * @return le nouveau port
	 */
	public static int getPort(String line) {
		String[] tab = line.split(",");
		return Integer.parseInt(tab[4]) * 256 + Integer.parseInt(tab[5]);
	}

	/**
	 * Methode qui recupere le retour de PASV et renvoie ce qu'il y'a entre
	 * parenthese
	 * 
	 * @param line le retour de la commande FTP PASV
	 * @return les informations entre parenthese
	 */
	public static String parsePASV(String line) {
		return line.substring(line.indexOf("(") + 1, line.indexOf(")"));
	}
}
