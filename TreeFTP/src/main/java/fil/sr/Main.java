package fil.sr;

import java.io.IOException;

import fil.sr.client.Authentification;
import fil.sr.client.Client;
import fil.sr.exception.AuthentificationServerException;
import fil.sr.exception.ChangeDirectoryException;
import fil.sr.exception.PassivModeException;

/**
 * Classe permettant l'execution de l'application
 * 
 * @author Bouali Hocine
 *
 */
public class Main {

	public static void help() {
		System.out.println(
				"usage: java -jar TreeFTP-1.0-SNAPSHOT.jar [adresse (ip |lien)] [nom d'utilisateur] [mot de passe] [[chiffre] profondeur]");
		System.out.println("Exemple d'utilisation : ");
		System.out.println("\t java -jar TreeFTP-1.0-SNAPSHOT.jar ftp.ubuntu.com ");
		System.out.println("\t java -jar TreeFTP-1.0-SNAPSHOT.jar ftp.ubuntu.com 3");
		System.out.println("\t java -jar TreeFTP-1.0-SNAPSHOT.jar ftp.ubuntu.com $nom $motdepasse");
		System.out.println("\t java -jar TreeFTP-1.0-SNAPSHOT.jar ftp.ubuntu.com $nom $motdepasse 2");

	}

	public static void main(String[] args)
			throws IOException, AuthentificationServerException, ChangeDirectoryException, PassivModeException {
		int len = args.length;
		Client c2;
		String serveur = "";
		Authentification auth = null;
		switch (len) {
		case 0:
			help();
			return;
		case 1:
			serveur = args[0];
			auth = new Authentification();
			c2 = new Client(auth);
			c2.connect(serveur, 21);
			c2.afficheALLDossier("", 3);
			break;
		case 2:

			serveur = args[0];
			auth = new Authentification();
			c2 = new Client(auth);
			c2.connect(serveur, 21);
			c2.afficheALLDossier("", Integer.parseInt(args[1]));
			break;
		case 3:
			serveur = args[0];
			auth = new Authentification(args[1], args[2]);
			c2 = new Client(auth);
			c2.connect(serveur, 21);
			c2.afficheALLDossier("", 3);
			break;
		case 4:
			serveur = args[0];
			auth = new Authentification(args[1], args[2]);
			c2 = new Client(auth);
			c2.connect(serveur, 21);
			c2.afficheALLDossier("", Integer.parseInt(args[3]));
			break;
		default:
			help();
			break;
		}

	}
}
