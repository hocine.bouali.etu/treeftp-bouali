package fil.sr.exception;

public class ChangeDirectoryException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ChangeDirectoryException() {
		super("failed to change directory");
	}

}
