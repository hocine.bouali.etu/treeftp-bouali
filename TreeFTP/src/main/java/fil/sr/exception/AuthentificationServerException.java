package fil.sr.exception;

public class AuthentificationServerException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public AuthentificationServerException() {
		super("Connexion au serveur refusée, verifier vos identifiants ");
	}
	public AuthentificationServerException(String msg) {
		super(msg);
	}
	

}
