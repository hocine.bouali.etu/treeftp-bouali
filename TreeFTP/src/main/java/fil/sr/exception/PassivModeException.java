package fil.sr.exception;

public class PassivModeException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public PassivModeException(String message) {
		super(message);
	}

}
