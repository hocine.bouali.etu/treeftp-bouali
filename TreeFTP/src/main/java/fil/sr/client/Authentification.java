package fil.sr.client;

/**
 * Classe qui represente un utilisateur par son nom d'utilisateur et son mot de
 * passe
 * 
 * @author Bouali Hocine
 *
 */
public class Authentification {
	private String user;
	private String psw;

	public Authentification(String user, String psw) {
		this.user = user;
		this.psw = psw;
	}

	public Authentification() {
		this.user = "anonymous";
		this.psw = "";
	}

	public String getUser() {
		return user;
	}

	public String getPsw() {
		return psw;
	}

}
