/**
 * Package contenant les classes permettant l'implementation du client FTP
 * 
 * @author Bouali Hocine
 *
 */
package fil.sr.client;