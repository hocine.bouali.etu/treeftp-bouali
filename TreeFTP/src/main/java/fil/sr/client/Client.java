package fil.sr.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import fil.sr.exception.AuthentificationServerException;
import fil.sr.exception.ChangeDirectoryException;
import fil.sr.exception.PassivModeException;
import fil.sr.utils.Utils;

/**
 * Classe Client qui permet de se connecter et de parcourir l'arborescence de
 * fichiers d'un serveur FTP
 * 
 * @author Bouali Hocine
 *
 */
public class Client {
	private Authentification auth;
	private Socket socket;
	private OutputStream out;
	private PrintWriter printer;
	private InputStream in;
	private InputStreamReader isr;
	private BufferedReader reader;
	private int nbFile = 0;
	private int nbFolder = 0;
	private String server;

	public Client(Authentification auth) {
		this.auth = auth;
	}

	/**
	 * Methode qui permet de se connecter au serveur d'adresse et de port passer en
	 * parametre
	 * 
	 * @param adresse l'adresse (IPv4 ou lien)
	 * @param port    le port
	 * @throws UnknownHostException            if the adresse is not valid
	 * @throws IOException
	 * @throws AuthentificationServerException if the username or password is false
	 */
	public void connect(String adresse, int port)
			throws UnknownHostException, IOException, AuthentificationServerException {
		String res;
		this.server = adresse;
		try {
			socket = new Socket(adresse, port);
		} catch (UnknownHostException e) {
			throw new UnknownHostException("Connexion au serveur refusée, adresse " + adresse + " inconnue");
		}

		out = socket.getOutputStream();
		printer = new PrintWriter(out, true);

		in = socket.getInputStream();
		isr = new InputStreamReader(in);
		reader = new BufferedReader(isr);

		res = reader.readLine();
		if (!res.startsWith("220")) {

			throw new UnknownHostException("Connexion au serveur refusée, adresse " + adresse + " inconnue");
		}
		res = sendMessageToServer("USER " + this.auth.getUser().trim() + "\r\n");

		res = sendMessageToServer("PASS " + this.auth.getPsw().trim() + "\r\n");
		if (!res.startsWith("230")) {
			throw new AuthentificationServerException(); // identifiants erronés
		}
	}

	/**
	 * Methode qui envoie les messages passer en parametre au serveur
	 * 
	 * @param message Un message a envoyer au serveur FTP
	 * @return renvoie la reponse du serveur
	 */
	private String sendMessageToServer(String message) {
		String res = "";
		try {
			printer.println(message);

			res = reader.readLine();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return res;
	}

	/**
	 * Methode qui parcours chaque sous dossier du dossier passer en aprametre et a
	 * comme effet de bord d'afficher leur arborescence
	 * 
	 * @param dir  le repertoire a parcourir en profondeur
	 * @param prof profondeur maximale de parcours
	 * @throws IOException                     si la reponse du serveur n'est pas
	 * @throws AuthentificationServerException si l'authentification a echouer
	 * @throws PassivModeException             si le serveur n'arrive pas a passer
	 *                                         en mode passif
	 */
	public void afficheALLDossier(String dir, int prof)
			throws IOException, AuthentificationServerException, PassivModeException {
		List<ListResponse> unDossier = new ArrayList<>();
		try {
			unDossier = afficheUnDossier(dir);
		} catch (ChangeDirectoryException e) {
			System.out.println("Impossible d'acceder a ce repertoire");
			return;
		}
		if (dir == "") {
			System.out.println("/");
		} else {
			System.out.println(dir);
		}
		if (unDossier.isEmpty()) {
			System.out.println("aucun dossier ou fichier dans ce repertoire");
			return;
		}
		for (ListResponse lr : unDossier) {
			if (lr.isRepository()) {
				nbFolder++;
				System.out.println("│    └── " + lr);
				explorer(dir + "/" + lr.getFileName(), "", prof - 1);
			} else if (lr.isLink()) {
				nbFolder++;
				System.out.println("│    └──" + lr);
			} else if (lr.isFile()) {
				nbFile++;
				System.out.println("│    └── " + lr);
			}
		}
		System.out.println("\n Fichiers : " + nbFile + "  Dossiers : " + nbFolder);
	}

	/**
	 * parcours en profondeur le repertoire passer en parametre eta comme effet de
	 * bord d'afficher son arborescence
	 * 
	 * @param dir   le repertoire a parcourir
	 * @param space le nombre d'espace a afficher pour respecter l'arborescence
	 * @param prof  la profondeur maximale de l'arborescence a afficher
	 * @throws IOException                     si la reponse du serveur n'est pas
	 * @throws AuthentificationServerException si l'authentification a echouer
	 * @throws PassivModeException             si le serveur n'arrive pas a passer
	 *                                         en mode passif
	 */
	private void explorer(String dir, String space, int prof)
			throws IOException, AuthentificationServerException, PassivModeException {
		if (prof > 0) {
			List<ListResponse> unDossier = new ArrayList<>();
			try {
				unDossier = afficheUnDossier(dir);
			} catch (ChangeDirectoryException e) {
				return;
			}
			space += "│    ";

			for (ListResponse lr : unDossier) {
				if (lr.isRepository()) {
					System.out.println(space + "│    └── " + lr.getFileName());
					nbFolder++;
					explorer(dir + "/" + lr.getFileName(), space, prof - 1);
				} else if (lr.isLink()) {
					nbFolder++;
					System.out.println(space + "│    └──" + lr.getFileName());
				} else if (lr.isFile()) {
					nbFile++;
					System.out.println(space + "│    └── " + lr.getFileName());
				}
			}
		} else {
			return;
		}
	}

	/**
	 * Methode qui renvoie le contenu du dossier passer en parametre
	 * 
	 * @param dir le repertoire a parcourir
	 * @return une liste de dossier/fichier du repertoire, renvoie null dans le cas
	 *         ou la commande CWD a echouer
	 * @throws IOException
	 * @throws AuthentificationServerException si l'authentification a echouer
	 * @throws ChangeDirectoryException        si le changement de repertoire est
	 *                                         impossible (user n'a pas les droits)
	 * @throws PassivModeException             si le serveur n'arrive pas a passer
	 *                                         en mode passif
	 */
	private List<ListResponse> afficheUnDossier(String dir)
			throws IOException, AuthentificationServerException, ChangeDirectoryException, PassivModeException {

		List<ListResponse> unDossier = new ArrayList<>();
		String res = "";
		try {

			res = sendMessageToServer("CWD /" + dir + "/" + "\r\n");

			if (res.startsWith("550")) { // Dans le cas ou l'utilisateur ne dispose pas des droits
				throw new ChangeDirectoryException();
			}
			sendMessageToServer("PWD " + "\r\n");

			res = sendMessageToServer("PASV " + "\r\n");
			if (!res.startsWith("227")) {
				throw new PassivModeException("echec du passage en mode passif: " + res);
			}
			int newPort = 0;
			String ip = null;

			if (res.toLowerCase().startsWith("227 entering passive mode")) {
				String temp = Utils.parsePASV(res);
				ip = Utils.getip(temp); // nouvelle adresse IP
				newPort = Utils.getPort(temp); // nouveau Port
				unDossier = getData(ip, newPort);
				res = reader.readLine();
			}
			res = reader.readLine();
		} catch (SocketException se) {
			this.connect(this.server, 21);// reconnexion en cas de deconnexion intenpestive par le serveur

		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return unDossier;
	}

	/**
	 * Methode qui va recuperer la sortie de la commande LIST
	 * 
	 * @param ip      l'adrersse ip pour le nouveau socket
	 * @param newPort le nouveau port d'ecoute
	 * @return renvoie sous forme de liste la reponse de LIST
	 */
	private List<ListResponse> getData(String ip, int newPort) {
		printer.println("LIST " + "\r\n");

		Socket s;
		List<ListResponse> unDossier = new ArrayList<>();
		try {

			s = new Socket(ip, newPort);
			InputStream in = s.getInputStream();
			InputStreamReader isr = new InputStreamReader(in);
			BufferedReader reader2 = new BufferedReader(isr);
			String line;
			while ((line = reader2.readLine()) != null) {
				unDossier.add(new ListResponse(line));
			}

			s.close();

		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return unDossier;
	}

}
