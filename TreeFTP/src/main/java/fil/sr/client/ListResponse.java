package fil.sr.client;

/**
 * Classe qui représente les données renvoyer par la commande FTP LIST
 * 
 * @author Bouali Hocine
 *
 */
public class ListResponse {

	private String response;
	private String permissions;
	private String fileName = "";
	private String size;

	public ListResponse(String list) {
		this.response = list;
		parse();
	}

	public boolean isLink() {
		return this.response.startsWith("l");
	}

	public boolean isRepository() {
		return this.response.startsWith("d");
	}

	public boolean isFile() {
		return this.response.startsWith("-");
	}

	@Override
	public String toString() {
		return this.fileName;
	}

	public String getPermissions() {
		return permissions;
	}

	public String getFileName() {
		return fileName;
	}

	public String getSize() {
		return size;
	}

	/**
	 * methode qui parse le retour de la commande LIST. Effet de bord qui affecte
	 * les permissions , la taille du fichier/dossier, et son nom
	 * 
	 */
	private void parse() {
		this.response = this.response.replaceAll("(\\s)+", " ");
		String[] parse = this.response.split(" ");
		this.permissions = parse[0];
		this.size = parse[4];
		if (parse.length > 9) {
			for (int i = 8; i < parse.length; i++) {

				this.fileName += " " + parse[i];
			}
		} else {
			this.fileName = parse[8];
		}

	}
}
