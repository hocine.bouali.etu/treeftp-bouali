## Parcours de l'arborescence de fichiers d'un serveur FTP

 Bouali Hocine
  		
 04/02/2021

#### Introduction

Cette application est inspirée de la commande shell [tree](https://fr.wikipedia.org/wiki/Tree_(commande)).

Cette application permet de récupérer l'arborescence de fichiers d'un serveur distant à travers le protocole [FTP](https://fr.wikipedia.org/wiki/File_Transfer_Protocol). L'algorithme utilisé est le [parcours en profondeur](https://fr.wikipedia.org/wiki/Algorithme_de_parcours_en_profondeur).

## Utilisation 

##### Etape 1: récupérer le projet

 Clone du dépôt dans le répertoire de votre choix

	$git clone https://gitlab.univ-lille.fr/hocine.bouali.etu/treeftp-bouali.git
 
 Placez-vous dans le bon dossier

	$cd treeftp-bouali/TreeFtp/

##### Etape 2: exécuter le projet

 Compilation du projet 
 
	$mvn package
 
 Cela va générer un *.jar* dans le dossier */target*
 
 Exécution du projet:
 
	$java -jar target/TreeFtp.jar

 L'application a besoin au minimum du nom d'un serveur FTP que l'on passe en argument

	$java -jar target/TreeFtp.jar ftp.free.fr

 Il est possible de renseigner la profondeur maximale du parcours (**vaudra 3 si aucun argument n'est passé**)

	$java -jar target/TreeFtp.jar ftp.free.fr 2

 Il est possible de renseigner un nom d'utilisateur et un mot de passe 

	$java -jar target/TreeFtp.jar ftp.free.fr user mdp
	$java -jar target/TreeFtp.jar ftp.free.fr user mdp 2
	
*NB: si un nom d'utilisateur est passé en paramètre, il est obligatoire de passer un mot de passe*


Une vidéo de démonstration est disponible dans *TreeFTP/doc/demo.mkv*

Le diagramme UML est également disponible dans *TreeFTP/doc/uml.png*

# Architecture

##### Gestion d'erreur

Lors d'une tentative de connexion à un serveur ftp , l'exception **UnknownHostException** est levée dans le cas où le serveur ne répond pas.
	
	public connect(String adress,int port){
		try {
			socket = new Socket(adresse, port);
		} catch (UnknownHostException e) {
			throw new UnknownHostException("Connexion au serveur refusée, adresse " + adresse + " inconnue");
		}
		...
	}

La vérification du nom d'utilisateur et du mot de passe peut générer une **AuthentificationServerException** qui indique que le mot de passe ou le nom d'utilisateur est erroné.
	
	public connect(String adress,int port){
		...
		res = sendMessageToServer("USER " + this.auth.getUser().trim() + "\r\n");
		res = sendMessageToServer("PASS " + this.auth.getPsw().trim() + "\r\n");
		if (!res.startsWith("230")) {
			throw new AuthentificationServerException();
		}
	}
	
Lors de la tentative d'un changement de répertoire , l'exception **ChangeDirectoryException** est levée si l'on ne peut pas accéder au répertoire.

	private List<ListResponse> afficheUnDossier(String dir){
		...
		res = sendMessageToServer("CWD /" + dir + "/" + "\r\n");
		
		if (res.startsWith("550")) {
			throw new ChangeDirectoryException();
		}
		...
	}
	
Si la connexion entre le serveur et le client est interrompue, l'exception **SocketException** est capturée, le client va alors tenter de se reconnecter automatiquement.

	private List<ListResponse> afficheUnDossier(String dir){
		try{
			...
		} catch (SocketException se) {
				this.connect(this.server, 21);
		}
	}
	
# Code Samples


  La méthode *afficheALLDossier* prend un chemin en paramètre ainsi que la profondeur maximale de parcours, elle affiche l'arborescence (fichier / dossier), si le repertoire passé en paramètre n'est pas accessible, l'algorithme s'arrête et affiche un message.

	public void afficheALLDossier(String dir, int prof)
				throws IOException, AuthentificationServerException, PassivModeException {
		List<ListResponse> unDossier = new ArrayList<>();
		try {
			unDossier = afficheUnDossier(dir); //recuperation de la liste des fichiers que contient le repertoire **dir**
		} catch (ChangeDirectoryException e){ //si le changement de reperoire est impossible
			...	    				//la methode se termine
			return;
		}
		...
		for (ListResponse lr : unDossier) {//parcours de tous les fichiers/dossier
			if (lr.isRepository()) {//s'il s'agit d'un repertoire , la methode fait un appel à la methode *explorer*
				System.out.println("│    └── " + lr);
				explorer(dir + "/" + lr.getFileName(), "", prof - 1);
			} else if (lr.isLink()) { //sinon on ne fait qu'afficher le fichier courant
				System.out.println("│    └──" + lr);
			} else if (lr.isFile()) {
				System.out.println("│    └── " + lr);
			}
		}
		...
	}

  La méthode *explorer* prend en paramètre un chemin , une chaine contenant des espaces et la profondeur restante à parcourir , la methode va parcourir en profondeur le dossier passé en paramètre et afficher l'arborescence  
	
	private void explorer(String dir, String space, int prof)
			throws IOException, AuthentificationServerException, PassivModeException {
		if (prof > 0) {//la methode teste si un parcours en profondeur est tjr possible
			List<ListResponse> unDossier = new ArrayList<>();
			try {
				unDossier = afficheUnDossier(dir); //recuperation de la liste des fichiers que contient le repertoire **dir**
			} catch (ChangeDirectoryException e) { //si le changement de reperoire est impossible
				return;    				//la methode se termine
			}
			space += "│    ";
			for (ListResponse lr : unDossier) { //parcours de tous les fichiers/dossier
				if (lr.isRepository()) { //s'il s'agit d'un repertoire , la methode fait un appel recursif avec le nouveau reperoire
					System.out.println(space + "│    └── " + lr.getFileName());
					explorer(dir + "/" + lr.getFileName(), space, prof - 1);
				} else if (lr.isLink()) {// sinon on ne fait qu'afficher les noms des fichiers 
					System.out.println(space + "│    └──" + lr.getFileName());
				} else if (lr.isFile()) {
					System.out.println(space + "│    └── " + lr.getFileName());
				}
			}
		} else {//sinon la fonction n'affiche rien et sort 
			return;
		}
	}

